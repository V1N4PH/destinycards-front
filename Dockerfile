FROM nginx:alpine
WORKDIR /usr/share/nginx/html
COPY css/style.css ./css/
COPY js/main.js ./js/
COPY img/logo.png ./img/
COPY index.html .
